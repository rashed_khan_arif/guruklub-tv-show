package com.rashed.guruklub.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rashed.guruklub.R
import com.rashed.guruklub.models.TVShow
import com.rashed.guruklub.network.APIClient
import com.squareup.picasso.MemoryPolicy

class TVShowAdapter(
    private var context: Context,
    var onItemClick: (id: Int) -> Unit,
    var onItemRemove: (position: Int) -> Unit
) :
    RecyclerView.Adapter<TVShowAdapter.TVShowHolder>() {
    private var items: MutableList<TVShow> = mutableListOf()

    fun addItems(items: MutableList<TVShow>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TVShowHolder {
        return TVShowHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.listitem_tv_show, parent, false)
        )
    }

    override fun onBindViewHolder(holder: TVShowHolder, position: Int) {
        holder.tvTitle.text = items[position].name
        holder.tvLanguage.text = items[position].language
        holder.tvGenre.text = items[position].getPrintableGenres()
        holder.setIsRecyclable(true)

        APIClient.getPicasso(context).load(items[position].image?.medium)
            .memoryPolicy(MemoryPolicy.NO_STORE).fit().into(holder.imgTvShow)

        holder.imgRemove.setOnClickListener {
            onItemRemove(position)
        }

        holder.itemView.setOnClickListener {
            onItemClick(items[position].id)
        }
    }

    fun removeItem(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, items.size)
    }

    override fun getItemCount() = items.size


    class TVShowHolder(view: View) : RecyclerView.ViewHolder(view) {
        var imgTvShow: ImageView = view.findViewById(R.id.imgTvShow)
        var tvTitle: TextView = view.findViewById(R.id.tvTitle)
        var tvLanguage: TextView = view.findViewById(R.id.tvLanguage)
        var tvGenre: TextView = view.findViewById(R.id.tvGenre)
        var imgRemove: ImageView = view.findViewById(R.id.imgRemove)
    }


}