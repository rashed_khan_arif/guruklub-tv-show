package com.rashed.guruklub.network

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.rashed.guruklub.config.ApiUrls
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class APIClient {
    companion object {

        fun getClient(): Retrofit {
            return Retrofit.Builder()
                .baseUrl(ApiUrls.BASE_URL)
                .client(getOkHttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .build()
        }

        private fun getGson(): Gson {
            return GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()
        }


        private fun getOkHttpClient(): OkHttpClient {
            val okb: OkHttpClient.Builder = OkHttpClient.Builder()
                .connectTimeout(20000, TimeUnit.MILLISECONDS)
            return okb.build()
        }


          fun getPicasso(context: Context): Picasso {
            return Picasso.Builder(context)
                .loggingEnabled(true)
                .downloader(OkHttp3Downloader(context, Integer.MAX_VALUE.toLong()))
                .build()
        }

    }


}