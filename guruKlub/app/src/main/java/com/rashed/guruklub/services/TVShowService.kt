package com.rashed.guruklub.services

import com.rashed.guruklub.config.ApiUrls
import com.rashed.guruklub.models.TVShow
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TVShowService {

    @GET(ApiUrls.TV_SHOWS)
    fun tvShows(): Single<List<TVShow>>

    @GET(ApiUrls.TV_SHOWS + "/{id}")
    fun tvShow(@Path("id") id: Int): Single<TVShow>
}