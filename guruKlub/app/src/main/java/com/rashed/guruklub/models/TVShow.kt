package com.rashed.guruklub.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class TVShow(
    @SerializedName("_links")
    @Expose
    val _links: Links?,
    @SerializedName("externals")
    @Expose
    val externals: Externals?,
    @SerializedName("genres")
    @Expose
    val genres: List<String>?,
    @SerializedName("id")
    @Expose
    val id: Int,
    @SerializedName("image")
    @Expose
    val image: Image?,
    @SerializedName("language")
    @Expose
    val language: String?,
    @SerializedName("name")
    @Expose
    val name: String?,
    @SerializedName("network")
    @Expose
    val network: Network?,
    @SerializedName("officialSite")
    @Expose
    val officialSite: String?,
    @SerializedName("premiered")
    @Expose
    val premiered: String?,
    @SerializedName("rating")
    @Expose
    val rating: Rating?,
    @SerializedName("runtime")
    @Expose
    val runtime: Int?,
    @SerializedName("schedule")
    @Expose
    val schedule: Schedule?,
    @SerializedName("status")
    @Expose
    val status: String?,
    @SerializedName("summary")
    @Expose
    val summary: String?,
    @SerializedName("type")
    @Expose
    val type: String?,
    @SerializedName("updated")
    @Expose
    val updated: Int?,
    @SerializedName("url")
    @Expose
    val url: String?,
    @SerializedName("webChannel")
    @Expose
    val webChannel: Any?,
    @SerializedName("weight")
    @Expose
    val weight: Int?
) {

    fun getPrintableGenres(): String {
        var data = ""
        genres?.forEach {
            data += "$it, "
        }
        return data.trim().trimEnd(',')
    }

    fun getSchedule(): String {
        var data = ""
        schedule?.days?.forEach {
            data += "$it: ${schedule.time} \n "
        }
        return data
    }

    fun getShowInfo(): String {
        return "<b>Premiered:</b> $premiered\n<b>Runtime:</b> $runtime\n<b>Type:</b> $type\n<b>Country:</b> ${network?.country?.name?:""}"
    }
}