package com.rashed.guruklub.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Image(
    @SerializedName("medium")
    @Expose
    val medium: String,
    @SerializedName("original")
    @Expose
    val original: String
)