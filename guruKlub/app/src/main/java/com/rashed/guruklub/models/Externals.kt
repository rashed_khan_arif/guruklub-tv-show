package com.rashed.guruklub.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Externals(
    @SerializedName("imdb")
    @Expose
    val imdb: String,
    @SerializedName("thetvdb")
    @Expose
    val thetvdb: Int,
    @SerializedName("tvrage")
    @Expose
    val tvrage: Int
)