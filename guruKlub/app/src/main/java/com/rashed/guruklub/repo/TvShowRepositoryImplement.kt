package com.rashed.guruklub.repo

import com.rashed.guruklub.models.TVShow
import com.rashed.guruklub.services.APIService
import com.rashed.guruklub.services.TVShowService
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class TvShowRepositoryImplement(var apiService: TVShowService) : ITvShowRepository {
    override fun getTvShows(): Single<List<TVShow>> {
        return apiService.tvShows().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getTvShow(id: Int): Single<TVShow> {
        return apiService.tvShow(id).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}