package com.rashed.guruklub.ui

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.RecyclerView
import com.rashed.guruklub.R
import com.rashed.guruklub.models.Rating
import com.rashed.guruklub.models.TVShow
import com.rashed.guruklub.network.APIClient
import com.rashed.guruklub.ui.adapters.TVShowAdapter
import com.squareup.picasso.MemoryPolicy

class TVShowActivity : BaseActivity() {

    private lateinit var imgTvShowImage: ImageView
    private lateinit var tvTitle: TextView
    private lateinit var tvGenre: TextView
    private lateinit var tvShowSchedule: TextView
    private lateinit var tvShowInfo: TextView
    private lateinit var tvLanguage: TextView
    private lateinit var tvRating: TextView
    private lateinit var toolbar: Toolbar
    private lateinit var tvShowSummary: TextView
    private lateinit var tvToolbarTitle: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_t_v_show)
        initializeUIComponents()
        getIntentExtras()
    }

    private fun initializeUIComponents() {
        imgTvShowImage = findViewById(R.id.imgTvShowImage)
        tvTitle = findViewById(R.id.tvTitle)
        tvGenre = findViewById(R.id.tvGenre)
        tvShowSchedule = findViewById(R.id.tvShowSchedule)
        tvShowInfo = findViewById(R.id.tvShowInfo)
        tvLanguage = findViewById(R.id.tvLanguage)
        tvRating = findViewById(R.id.tvRating)
        tvShowSummary = findViewById(R.id.tvShowSummary)
        toolbar = findViewById(R.id.toolbar)
        tvToolbarTitle = findViewById(R.id.tvToolbarTitle)
        configToolbar()
    }

    private fun configToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

    }

    private fun getIntentExtras() {
        val tvShowId: Int = intent.getIntExtra(ARG_TVSHOW_ID, 0)
        getTvShow(tvShowId)
    }


    private fun getTvShow(id: Int) {
        disposable?.add(tvShowRepository().getTvShow(id).subscribe({
            bindTVShow(it)
        }, this::showErrorMessage))
    }

    private fun bindTVShow(tvShow: TVShow) {
        with(tvShow) {
            tvTitle.text = name ?: ""
            tvGenre.text = getPrintableGenres()
            image?.let {
                bindTvShowImage(it.original)
            }
            tvLanguage.text = language
            tvShowSchedule.text = getSchedule()
            tvShowInfo.text = Html.fromHtml(getShowInfo())
            tvShowSummary.text = Html.fromHtml(summary ?: "")
            rating?.let {
                bindRating(it)
            }
            tvToolbarTitle.text = name
        }
    }

    private fun bindRating(rating: Rating) {
        if (rating.average > 0) {
            tvRating.visibility = View.VISIBLE
            tvRating.text = rating.average.toString()
        } else {
            tvRating.visibility = View.GONE
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun bindTvShowImage(url: String) {
        APIClient.getPicasso(this).load(url)
            .memoryPolicy(MemoryPolicy.NO_STORE).fit().into(imgTvShowImage)
    }


    companion object {
        private const val ARG_TVSHOW_ID = "tv_show_id"

        fun start(context: Context, id: Int) {
            val intent = Intent(context, TVShowActivity::class.java)
            intent.putExtra(ARG_TVSHOW_ID, id)
            context.startActivity(intent)

        }

    }



    override fun onDestroy() {
        super.onDestroy()
        disposable?.dispose()
    }
}