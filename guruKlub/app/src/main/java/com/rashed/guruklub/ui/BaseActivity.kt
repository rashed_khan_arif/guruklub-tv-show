package com.rashed.guruklub.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.rashed.guruklub.repo.ITvShowRepository
import com.rashed.guruklub.repo.TvShowRepositoryImplement
import com.rashed.guruklub.services.APIService
import com.rashed.guruklub.services.TVShowService
import io.reactivex.disposables.CompositeDisposable

open class BaseActivity : AppCompatActivity() {
    protected var disposable: CompositeDisposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        disposable = CompositeDisposable()
    }

    fun tvShowRepository(): ITvShowRepository {
        val apiService = APIService.get(TVShowService::class.java)
        return TvShowRepositoryImplement(apiService)
    }

    protected fun showErrorMessage(error: Throwable) {
        val data = error
    }
}