package com.rashed.guruklub.ui

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rashed.guruklub.R
import com.rashed.guruklub.models.TVShow
import com.rashed.guruklub.repo.ITvShowRepository
import com.rashed.guruklub.repo.TvShowRepositoryImplement
import com.rashed.guruklub.services.APIService
import com.rashed.guruklub.services.TVShowService
import com.rashed.guruklub.ui.adapters.TVShowAdapter
import io.reactivex.disposables.CompositeDisposable

class MainActivity : BaseActivity() {

    private lateinit var rcvTvShows: RecyclerView
    private lateinit var adapter: TVShowAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializeUIComponents()
        getTvShows()
    }

    private fun initializeUIComponents() {
        rcvTvShows = findViewById(R.id.rcvTvShows)
        adapter = TVShowAdapter(this, this::onItemClick, this::onItemRemove)
        initTvShowAdapter()
    }


    private fun initTvShowAdapter() {
        rcvTvShows.adapter = adapter
        rcvTvShows.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rcvTvShows.setHasFixedSize(true)
        rcvTvShows.itemAnimator = DefaultItemAnimator()
    }


    private fun getTvShows() {
        disposable?.add(tvShowRepository().getTvShows().subscribe({
            adapter.addItems(it.toMutableList())
        }, this::showErrorMessage))
    }


    private fun onItemClick(id: Int) {
        TVShowActivity.start(this, id)
    }

    private fun onItemRemove(position: Int) {
        AlertDialog.Builder(this).setMessage("Do you really want to remove the entry from List ?")
            .setPositiveButton(
                "Yes"
            ) { _, _ -> adapter.removeItem(position) }.setNegativeButton("No", null).show()
    }

}