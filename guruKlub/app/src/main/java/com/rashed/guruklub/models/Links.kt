package com.rashed.guruklub.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Links(
    @SerializedName("previousepisode")
    @Expose
    val previousepisode: Previousepisode,
    @SerializedName("self")
    @Expose
    val self: Self
)