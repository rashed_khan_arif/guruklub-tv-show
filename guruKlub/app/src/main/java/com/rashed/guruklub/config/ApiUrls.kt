package com.rashed.guruklub.config

object ApiUrls {
    const val BASE_URL: String = "http://api.tvmaze.com"
    const val TV_SHOWS: String = "/shows"
}