package com.rashed.guruklub.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Country(
    @SerializedName("code")
    @Expose
    val code: String,
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("timezone")
    @Expose
    val timezone: String
)