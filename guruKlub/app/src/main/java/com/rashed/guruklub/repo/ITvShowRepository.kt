package com.rashed.guruklub.repo

import com.rashed.guruklub.models.TVShow
import io.reactivex.Single

interface ITvShowRepository {
    fun getTvShows(): Single<List<TVShow>>
    fun getTvShow(id: Int): Single<TVShow>
}