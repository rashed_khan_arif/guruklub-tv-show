package com.rashed.guruklub.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Schedule(
    @SerializedName("days")
    @Expose
    val days: List<String>,
    @SerializedName("time")
    @Expose
    val time: String
)