package com.rashed.guruklub.services

import com.rashed.guruklub.network.APIClient

object APIService {

    fun <T> get(apiClass: Class<T>): T {
        return APIClient.getClient().create(apiClass)
    }

}