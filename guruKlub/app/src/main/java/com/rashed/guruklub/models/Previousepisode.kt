package com.rashed.guruklub.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Previousepisode(
    @SerializedName("href")
    @Expose
    val href: String
)